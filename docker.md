# Docker

## [Back README](./README.md)

## What is Docker?

## Install

### 1. Remove Older version of Docker 

```{r, engine="bash", count_lines}
  sudo apt-get remove docker docker-engine docker.io containerd runc
```

### 2. Setup dependency

* Update the apt package index and install packages to allow apt to use a repository over HTTPS

```{r, engine="bash", count_lines}
  sudo apt-get update
```

```{r, engine="bash", count_lines}
  sudo apt-get install install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

* Add Docker’s official GPG key:

```{r, engine="bash", count_lines}
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

* Use the following command to set up the stable repository. To add the nightly or test repository, add the word nightly or test (or both) after the word stable in the commands below

```{r, engine="bash", count_lines}
  echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

* Install docker

```{r, engine="bash", count_lines}
  sudo apt-get update && \
  sudo apt-get install docker-ce docker-ce-cli containerd.io
```
## [Back README](./README.md)