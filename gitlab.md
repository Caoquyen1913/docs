# **Gitlab**

## [Back README](./README.md)

## What is Gitlab?

Gitlab is an open source selt-hosted system based on the Git server system used to manage source code.

## Version

**Gitlab Commnunity Edition (Gitlab-ce):**
 open source

**Gitlab Enterprise Edition (Gitlab-ee)**
 gitlab enterprise version. When the company registers gitlab, they will receive Gitlab BV's support when they encounter difficulties in the installation and use process.

 **Gitlab Continuous Integration**

 ![Gitlab ce vs ee](https://www.almtoolbox.com/blog/wp-content/uploads/2020/08/gitlab-tiers-repos-and-tiers-768x437.jpg)

 ![Gitlab Pricing](./images/pricing.png)

## Install

### Docker

#### 1. Install docker

[install docker](./docker.md)

#### 2. Setup volume

* Linux

```bash
export GITLAB_HOME=/srv/gitlab
```

* Macos

```bash
export GITLAB_HOME=$HOME/gitlab
```

#### 3. Pull Gitlab

```bash
docker pull gitlab/gitlab-ce
```

#### 4. Run container

```bash
 docker run --detach \
  --publish 8000:80 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ce:latest
```

The initialization process may take a long time. You can track this process with:

```bash
docker logs -f gitlab
```

#### 5. Get root password

```bash
docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
```

#### 6. Get Gitlab by Browser

Open Browser and get <https://localhost:8000> in server.

Login as:

username: `root`

password: `get password in path 5`

#### 7. Setup by root permission

#### Note: Connect in other devices and Login by normal account

#### 1. Get Gitlab by Browser

 get <http://serverip:8000> in other devices

 If has account:

* Login by your username and password

If doesn't have account:

* Register account

* Login by account that have registered

### Docker compose

#### 1. Install docker and docker-compose

[install docker](./docker.md)

#### 2. Setup volume

* Linux

```bash
export GITLAB_HOME=/srv/gitlab
```

* Macos

```bash
export GITLAB_HOME=$HOME/gitlab
```

#### 3. Setup file `docker-compose.yml`

```dockerfile
version: '3.6'
services:
  web:
    image: 'gitlab/gitlab-ee:latest'
    restart: always
    hostname: 'gitlab.example.com'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'https://gitlab.example.com'
        # Add any other gitlab.rb configuration here, each on its own line
    ports:
      - '8000:80'
    volumes:
      - '$GITLAB_HOME/config:/etc/gitlab'
      - '$GITLAB_HOME/logs:/var/log/gitlab'
      - '$GITLAB_HOME/data:/var/opt/gitlab'
    shm_size: '256m'
```

#### 4. Run docker-compose

```bash
docker-compose up -d
```

## Uninstall

### Docker

#### 1. Show container is running

```bash
docker ps
```

```bash
hq@name:~$ docker ps
CONTAINER ID   IMAGE                         COMMAND                  CREATED          STATUS                      PORTS                                                                                                                   NAMES
623338c39c93   gitlab/gitlab-ce:latest       "/assets/wrapper"        2 hours ago      Up 31 minutes (unhealthy)   22/tcp, 0.0.0.0:8888->80/tcp, :::8888->80/tcp, 443/tcp,    gitlab
```

### 2. Stop and remove container 

```bash
docker stop CONTAINER_ID && docker rm CONTAINER_ID
```
## [Back README](./README.md)